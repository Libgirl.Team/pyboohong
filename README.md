The Python repo for imitation learning development. Imitation = 模仿 = Bôo-hóng in Taiwanese.

# NN Model

The latest model is the `PVLTMN` in `PyBooHong/esn/vltmn.py`.

## Descriptions of functions

`evolve`: evolve for a specific time step dt.
`forward`: produce network output of the current state.
`hebb_learn`: Hebbian learning.
`hebb_panic`: when the NN doesn't act correctly, make random perturbation on its states. The amplitude of perturbation is propotional to the sum of the current amplitude of Hebbian learning of the incoming connections.

# Usage

## Run by Python script

 1. Write your script in `PyBooHong/scripts`

for example, `PyBooHong/scripts/try_exe.py`

 2. activate the conda env with packages installed
 3. in root folder of PyBooHong, run `python3 entry.py try_exe`

Then logs will be saved in `logs`. with the `run_n_save` in `PyBooHong/proc/pvltmn_ope`, models & data will be saved undewr `store`.

