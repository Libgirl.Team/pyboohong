import torch
from torch import nn
from torch import matmul
from torch import mul
from torch import randn
from torch.nn import Parameter
import math

class SampleImitator(nn.Module):
    def __init__(self, n_tch_in, n_out, tau_out):
        super(SampleImitator, self).__init__()
        self.n_tch_in = n_tch_in
        self.n_out = n_out
        self.set_init_state()
        self.tau_out = tau_out
        # self.acce_rate = acce_rate
        # self.c_out = self.acce_rate / self.tau_out
        
    def set_init_state(self): # should not be randn!!
        self.state_out_v = Parameter(torch.zeros(self.n_out))
        
    def forward(self, dt, tch_in, state_env):
        state_env = state_env.add(self.state_out_v.mul(dt))
        self.state_out_v = Parameter(
            (self.state_out_v + (- self.state_out_v / self.tau_out + tch_in) * dt).detach()
        )
        return state_env
        

class EchoImitator(nn.Module):
    def __init__(self, n_tch_in, n_inf_in, n_rsvr, n_out,
                 tau_rsvr, tau_frc, tau_velo, tau_oja, mass_out,
                 fn_rsvr):
        
        super(EchoImitator, self).__init__()
        self.n_tch_in = n_tch_in
        self.n_inf_in = n_inf_in
        self.n_rsvr = n_rsvr
        self.n_out = n_out
        rho2_b_r = 1e-0
        sigma_b_r = math.sqrt(2 * rho2_b_r)
        self.b_to_r = Parameter(randn(1, self.n_rsvr) * sigma_b_r)
        self.oja_sum_b_r = Parameter(torch.zeros(1, self.n_rsvr))
        rho2_ti_r = 9.0e-0
        sigma_ti_r = math.sqrt(2 * rho2_ti_r) / self.n_tch_in
        self.ti_to_r = Parameter(randn(self.n_tch_in, self.n_rsvr) * sigma_ti_r)
        self.oja_sum_ti_r = Parameter(torch.zeros(self.n_tch_in, self.n_rsvr))
        rho2_ii_r = 1e-0
        sigma_ii_r = math.sqrt(2 * rho2_ii_r) / self.n_inf_in
        self.ii_to_r = Parameter(randn(self.n_inf_in, self.n_rsvr) * sigma_ii_r)
        self.oja_sum_ii_r = Parameter(torch.zeros(self.n_inf_in, self.n_rsvr))
        rho2_r_r = 1e-0
        sigma_r_r = math.sqrt(2 * rho2_r_r) / self.n_rsvr
        self.r_to_r = Parameter(randn(self.n_rsvr, self.n_rsvr) * sigma_r_r)
        self.oja_sum_r_r = Parameter(torch.zeros(self.n_rsvr, self.n_rsvr))
        rho2_f_r = 1e-0
        sigma_f_r = math.sqrt(2 * rho2_f_r) / self.n_out
        self.f_to_r = Parameter(randn(self.n_out, self.n_rsvr) * sigma_f_r)
        self.oja_sum_f_r = Parameter(torch.zeros(self.n_out, self.n_rsvr))
        rho2_e_r = 1e-0
        sigma_e_r = math.sqrt(2 * rho2_e_r) / self.n_out
        self.e_to_r = Parameter(randn(self.n_out, self.n_rsvr) * sigma_e_r)
        self.oja_sum_e_r = Parameter(torch.zeros(self.n_out, self.n_rsvr))
        rho2_r_f = 1e-0
        sigma_r_f = math.sqrt(2 * rho2_r_f) / self.n_rsvr
        self.r_to_f = Parameter(randn(self.n_rsvr, self.n_out, requires_grad=True) * sigma_r_f)
        self.oja_sum_r_f = Parameter(torch.zeros(self.n_rsvr, self.n_out))
        
        self.set_init_state()
        self.tau_rsvr = tau_rsvr
        self.tau_velo = tau_velo
        self.tau_frc = tau_frc
        self.tau_oja = tau_oja
        self.mass_out = mass_out
        self.fn_rsvr = fn_rsvr
    
    def set_init_state(self): # should not be randn!!
        self.state_rsvr = Parameter(randn(1, self.n_rsvr))
        self.state_f = Parameter(randn(1, self.n_out))
        self.state_v = Parameter(torch.zeros(1, self.n_out))
        self.efficacy_rsvr = Parameter(torch.zeros(1, self.n_rsvr))
        self.efficacy_ti = Parameter(torch.zeros(1, self.n_tch_in))
        self.efficacy_ii = Parameter(torch.zeros(1, self.n_inf_in))
        self.efficacy_b = Parameter(torch.zeros(1, self.n_rsvr))
        self.efficacy_f = Parameter(torch.zeros(1, self.n_out))
        self.efficacy_e = Parameter(torch.zeros(1, self.n_out))

    def update_efficacy(self, dt, tch_in, inf_in, state_env, signal_r_to_f):
        self.efficacy_rsvr = Parameter(
            self.efficacy_rsvr + dt * (- self.efficacy_rsvr / self.tau_rsvr + signal_r_to_f)
        )
        self.efficacy_ti = Parameter(self.efficacy_ti + dt * (- self.efficacy_ti / self.tau_rsvr + tch_in))
        self.efficacy_ii = Parameter(self.efficacy_ii + dt * (- self.efficacy_ii / self.tau_rsvr + inf_in))
        self.efficacy_b = Parameter(self.efficacy_b + dt * (- self.efficacy_b / self.tau_rsvr + self.b_to_r))
        self.efficacy_f = Parameter(
            self.efficacy_f + dt * (- self.efficacy_f / self.tau_rsvr + self.state_f)
        )
        self.efficacy_e = Parameter(self.efficacy_e + dt * (- self.efficacy_e / self.tau_rsvr + state_env))

    def update_oja_sum(self, dt):
        self.oja_sum_b_r = Parameter(
            self.oja_sum_b_r +
            dt * (- self.oja_sum_b_r / self.tau_oja +
                  oja_eq_dim(self.efficacy_b, self.state_rsvr, self.b_to_r))
        )
        self.oja_sum_ti_r = Parameter(
            self.oja_sum_ti_r +
            dt * (- self.oja_sum_ti_r / self.tau_oja + oja(self.efficacy_ti, self.state_rsvr, self.ti_to_r))
        )
        self.oja_sum_ii_r = Parameter(
            self.oja_sum_ii_r +
            dt * (- self.oja_sum_ii_r / self.tau_oja + oja(self.efficacy_ii, self.state_rsvr, self.ii_to_r))
        )
        self.oja_sum_r_r = Parameter(
            self.oja_sum_r_r +
            dt * (- self.oja_sum_r_r / self.tau_oja + oja(self.efficacy_rsvr, self.state_rsvr, self.r_to_r))
        )
        self.oja_sum_f_r = Parameter(
            self.oja_sum_f_r +
            dt * (- self.oja_sum_f_r / self.tau_oja + oja(self.efficacy_f, self.state_rsvr, self.f_to_r))
        )
        self.oja_sum_e_r = Parameter(
            self.oja_sum_e_r +
            dt * (- self.oja_sum_e_r / self.tau_oja + oja(self.efficacy_e, self.state_rsvr, self.e_to_r))
        )
        self.oja_sum_r_f = Parameter(
            self.oja_sum_r_f +
            dt * (- self.oja_sum_r_f / self.tau_oja + oja(self.efficacy_rsvr, self.state_f, self.r_to_f))
        )

    def oja_learn(self, dt, rate):
        self.b_to_r = Parameter(self.b_to_r + self.oja_sum_b_r * dt * rate)
        self.ti_to_r = Parameter(self.ti_to_r + self.oja_sum_ti_r * dt * rate)
        self.ii_to_r = Parameter(self.ii_to_r + self.oja_sum_ii_r * dt * rate)
        self.r_to_r = Parameter(self.r_to_r + self.oja_sum_r_r * dt * rate)
        self.f_to_r = Parameter(self.f_to_r + self.oja_sum_f_r * dt * rate)
        self.e_to_r = Parameter(self.e_to_r + self.oja_sum_e_r * dt * rate)
        self.r_to_f = Parameter(self.r_to_f + self.oja_sum_r_f * dt * rate)

    def oja_panic(self, dt, rate):
        self.state_rsvr = Parameter(
            self.state_rsvr - dt * rate * (
                self.oja_sum_b_r * (randn(1, self.n_rsvr) + 1) +
                matmul(torch.ones(self.n_tch_in),
                       self.oja_sum_ti_r * (randn(self.n_tch_in, self.n_rsvr) + 1)) +
                matmul(torch.ones(self.n_inf_in),
                       self.oja_sum_ii_r * (randn(self.n_inf_in, self.n_rsvr) + 1)) +
                matmul(torch.ones(self.n_rsvr),
                       self.oja_sum_r_r * (randn(self.n_rsvr, self.n_rsvr) + 1)) +
                matmul(torch.ones(self.n_out),
                       self.oja_sum_f_r * (randn(self.n_out, self.n_rsvr) + 1)) +
                matmul(torch.ones(self.n_out),
                       self.oja_sum_e_r * (randn(self.n_out, self.n_rsvr) + 1))
            )
        )
        self.state_f = Parameter(
            self.state_f - dt * rate * matmul(
                torch.ones(self.n_rsvr), self.oja_sum_r_f * (randn(self.n_rsvr, self.n_out) + 1)
            )
        )
        
    def forward(self, dt, tch_in, inf_in, state_env, is_running_oja):
        rate_rsvr = (- self.state_rsvr / self.tau_rsvr +
                     self.b_to_r +
                     matmul(self.fn_rsvr(self.state_rsvr), self.r_to_r) +
                     matmul(self.state_f, self.f_to_r) +
                     matmul(tch_in, self.ti_to_r) +
                     matmul(inf_in, self.ii_to_r) +
                     matmul(state_env, self.e_to_r))
        signal_r_to_f = self.fn_rsvr(self.state_rsvr)
        rate_f = (- self.state_f / self.tau_frc + matmul(signal_r_to_f, self.r_to_f))
        # ODE need improvement, e.g. use RK4
        self.state_rsvr = Parameter(self.state_rsvr.add(rate_rsvr.mul(dt)).detach())
        state_env = state_env.add(self.state_v.mul(dt))
        self.state_v = Parameter(
            (self.state_v +
             (- self.state_v / self.tau_velo + self.state_f) / self.mass_out * dt)
            .detach()
        )
        return_state_f = self.state_f.add(rate_f.mul(dt))
        #return_state_out_a = matmul(signal_r_to_oa, self.r_to_oa)
        # print('rsvr: {:.3E}, out: {:.3E}, velo_out: {:.3E}, state_env: {:.3E}, velo_env: {:.3E}.'.format(
        #     self.state_rsvr.abs().mean(),
        #     self.state_out_a.abs().mean(),
        #     rate_out_a.abs().mean(),
        #     state_env.abs().mean(),
        #     self.state_out_v.abs().mean()
        # ))
        self.state_f = Parameter(return_state_f.detach())
        if is_running_oja:
            self.update_efficacy(dt, tch_in, inf_in, state_env, signal_r_to_f)
            self.update_oja_sum(dt)
        return state_env, return_state_f, signal_r_to_f.detach()

