import torch
from torch import nn
from torch import matmul
from torch import mul
from torch import randn
from torch.nn import Parameter
from .components import BiasHbWts, InterHbWts, Efficacy1D as Efficacy, Reservoir, perturb

class TeacherFollower(nn.Module):
    # single teach_in, single-out. inf_in inject on each mtr_neuron.
    def __init__(self,
                 n_tch_rsvr, n_mtr_rsvr,
                 sgm_ti_tr, sgm_tr_tr, sgm_tr_mr, sgm_mr_mr, sgm_mr_o, sgm_perturb,
                 tau_oja, tau_tr, tau_mr,
                 fn_r, up_bnd, low_bnd):
        super(TeacherFollower, self).__init__()
        # self.n_tch_rsvr = n_tch_rsvr
        # self.n_mtr_rsvr = n_mtr_rsvr
        self.tau_tr = tau_tr
        self.tau_mr = tau_mr
        self.w_ti_tr = InterHbWts(1, n_tch_rsvr, sgm_ti_tr, tau_oja)
        self.eff_ti_tr = Efficacy(1, tau_tr)
        self.w_b_mr = BiasHbWts(n_mtr_rsvr, sgm_mr_mr, tau_oja)
        self.eff_b_mr = Efficacy(n_mtr_rsvr, tau_mr)
        self.w_tr_mr = InterHbWts(n_tch_rsvr, n_mtr_rsvr, sgm_tr_mr, tau_oja)
        self.eff_tr_mr = Efficacy(n_tch_rsvr, tau_mr)
        self.w_mr_o = InterHbWts(n_mtr_rsvr, 1, sgm_mr_o, tau_oja) # instant; pre_signal = pre_eff
        self.rsvr_t = Reservoir(n_tch_rsvr, tau_tr, fn_r, up_bnd, low_bnd, sgm_tr_tr, sgm_perturb, tau_oja)
        self.rsvr_m = Reservoir(n_mtr_rsvr, tau_mr, fn_r, up_bnd, low_bnd, sgm_mr_mr, sgm_perturb, tau_oja)

    def forward(self):
        return matmul(self.rsvr_m.signal(), self.w_mr_o.w)

    def evolve(self, dt, tch_in, inf_in, is_running_hebb):
        # forward signal & evolve.
        signal_tch = self.rsvr_t.signal()
        signal_mtr = self.rsvr_m.signal()
        signal_o = matmul(signal_mtr, self.w_mr_o)
        
        if is_running_hebb:
            # update hebb
            self.w_ti_tr.update_hebb(dt, self.eff_ti_tr.state, self.rsvr_t.state)
            self.w_tr_mr.update_hebb(dt, self.eff_tr_mr.state, self.rsvr_m.state)
            self.w_b_mr.update_hebb(dt, self.eff_b_mr.state, self.rsvr_m.state)
            self.w_mr_o.update_hebb(dt, signal_mtr, signal_o)
            # udpate efficacy
            self.eff_ti_tr.upd(dt, tch_in)
            self.eff_tr_mr.upd(dt, signal_tch)
            self.eff_b_mr.upd(dt, self.w_b_mr.w)
            
        self.rsvr_t.evolve(dt, matmul(tch_in, self.w_ti_tr.w), is_running_hebb)
        self.rsvr_m.evolve(
            dt,
            inf_in + matmul(signal_tch, self.w_tr_mr.w) + self.w_b_mr.w,
            is_running_hebb
        )

    def hebb_learn(self, dosage):
        self.rsvr_t.w_intra.hebb_learn(dosage)
        self.rsvr_m.w_intra.hebb_learn(dosage)
        self.w_ti_tr.hebb_learn(dosage)
        self.w_tr_mr.hebb_learn(dosage)
        self.w_b_mr.hebb_learn(dosage)
        self.w_mr_o.hebb_learn(dosage)
        
    def hebb_panic(self, dt, rate):
        self.rsvr_t.hebb_panic(dt, rate, self.w_ti_tr.hebb_out())
        self.rsvr_m.hebb_panic(dt, rate, self.w_tr_mr.hebb_out())        
