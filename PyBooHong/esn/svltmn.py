import torch
from torch import nn
from torch import matmul
from torch import mul
from torch import randn
from torch.nn import Parameter
from .components import BiasHbWts, InterHbWts, Reservoir, perturb, Efficacy2D

class SensorimotorNet():
    def __init__(self,
                 n_ii, n_sr, n_vr,
                 genw_ii_sr, genw_eti_sr, genw_b_sr, genw_sr_vr, genw_vr_sr, genw_sr_o, genw_sr_sr,
                 sgm_panic,
                 tau_hebb, gen_tau_sr, tau_vr,
                 fn_r, up_bnd, low_bnd, rate_noise):
        self.r_s = Reservoir(n_sr, gen_tau_sr, fn_r, up_bnd, low_bnd,
                             genw_sr_sr, sgm_panic, tau_hebb, rate_noise)
        self.w_ii_sr = InterHbWts(n_ii, n_sr, genw_ii_sr, tau_hebb) # inference_input
        self.eff_ii_sr = Efficacy2D(n_ii, self.r_s.tau_dyn)
        self.w_eti_sr = InterHbWts(3, n_sr, genw_eti_sr, tau_hebb) # 1st: env; 2nd: tch 1/0, 2rd: tch signal.
        self.eff_eti_sr = Efficacy2D(3, self.r_s.tau_dyn)
        self.w_b_sr = BiasHbWts(n_sr, genw_b_sr, tau_hebb)
        self.eff_b_sr = Efficacy2D(n_sr, self.r_s.tau_dyn)
        self.w_sr_vr = InterHbWts(n_sr, n_vr, genw_sr_vr, tau_hebb)
        self.eff_sr_vr = Efficacy2D(n_sr, tau_vr)
        self.w_vr_sr = InterHbWts(n_vr, n_sr, genw_vr_sr, tau_hebb)
        self.eff_vr_sr = Efficacy2D(n_vr, self.r_s.tau_dyn)
        self.w_sr_o = InterHbWts(n_sr, 1, genw_sr_o, tau_hebb)
        
    def evolve(self, dt, inf_in, envtch_in, vltmn_in, vltmn_state, is_running_hebb):
        signal_sr = self.r_s.signal()
        signal_o = matmul(signal_sr, self.w_sr_o)
        if is_running_hebb:
            self.w_ii_sr.update_hebb(dt, self.eff_ii_sr.state, self.r_s.state)
            self.w_eti_sr.update_hebb(dt, self.eff_eti_sr.state, self.r_s.state)
            self.w_b_sr.update_hebb(dt, self.eff_b_sr.state, self.r_s.state)
            self.w_sr_vr.update_hebb(dt, self.eff_sr_vr, vltmn_state)
            self.w_vr_sr.update_hebb(dt, self.eff_vr_sr, self.r_s.state)
            self.w_sr_o.update_hebb(dt, signal_sr, signal_o)
            self.eff_ii_sr.upd(dt, inf_in)
            self.eff_eti_sr.upd(dt, envtch_in)
            self.eff_b_sr.upd(dt, self.w_b_sr.w)
            self.eff_sr_vr.upd(dt, signal_sr)
            self.eff_vr_sr.upd(dt, vltmn_in)
        self.r_s.evolve(dt,
                        matmul(inf_in, self.w_ii_sr.w) + self.w_b_sr.w + matmul(vltmn_in, self.w_vr_sr.w),
                        is_running_hebb)

    def signal_to_vltm(self):
        return matmul(self.r_s.signal(), self.w_sr_vr).detach()
        
    def forward(self):
        return matmul(self.r_s.signal(), self.w_sr_o)
        
    def hebb_learn(self, dosage):
        self.w_ii_sr.hebb_learn(dosage)
        self.w_eti_sr.hebb_learn(dosage)
        self.w_b_sr.hebb_learn(dosage)
        self.w_sr_vr.hebb_learn(dosage)
        self.w_vr_sr.hebb_learn(dosage)
        self.w_sr_o.hebb_learn(dosage)
        self.r_s.hebb_learn(dosage)
        
    def hebb_panic(self, dt, rate):
        self.r_s.hebb_panic(rate,
                            dt,
                            self.w_b_sr.hebb_out() + self.w_ii_sr.hebb_out() + self.w_vr_sr.hebb_out())

class SVLTMN(nn.Module):
    def __init__(self,
                 n_ii, n_sr, n_vr, n_out,
                 genw_ii_sr, genw_eti_sr, genw_b_sr, genw_sr_vr, genw_vr_sr, genw_sr_o, genw_sr_sr,
                 genw_ii_vr, genw_eti_vr, genw_b_vr, genw_vr_vr,
                 tau_hebb, sgm_panic, gen_tau_sr, gen_tau_vr,
                 fn_r, up_bnd, low_bnd, rate_noise):
        self.r_v = Reservoir(n_vr, gen_tau_vr, fn_r, up_bnd, low_bnd,
                             genw_vr_vr, sgm_panic, tau_hebb, rate_noise)
        self.ssrmtr_nets = [SensorimotorNet(
            n_ii, n_sr, n_vr,
            genw_ii_sr, genw_eti_sr, genw_b_sr, genw_sr_vr, genw_vr_sr, genw_sr_o, genw_sr_sr, sgm_panic,
            tau_hebb, gen_tau_sr, self.r_v.tau_dyn,
            fn_r, up_bnd, low_bnd, rate_noise
        ) for net in range(n_out)]
        self.w_ii_vr = InterHbWts(n_ii, n_vr, genw_ii_vr, tau_hebb)
        self.eff_ii_vr = Efficacy2D(n_ii, self.r_v.tau_dyn)
        self.w_eti_vr = InterHbWts(2 * n_out, n_vr, genw_eti_vr, tau_hebb)
        self.eff_eti_vr = Efficacy2D(2 * n_out, self.r_v.tau_dyn)
        self.w_b_vr = BiasHbWts(n_vr, genw_b_vr, tau_hebb)
        self.eff_b_vr = Efficacy2D(n_vr, self.r_v.tau_dyn)

    def evolve(self, dt, inf_in, envtch_in, is_running_hebb):
        # all the inputs are normalized before injection.
        vltmn_signal = self.r_v.signal()
        vltmm_state = self.r_v.state
        signal_s_v = sum([net.signal_to_vltm() for net in self.ssrmtr_nets]).detach()
        
        if is_running_hebb:
            self.w_ii_vr.update_hebb(dt, self.eff_ii_vr.state, self.r_v.state)
            self.w_eti_vr.update_hebb(dt, self.eff_eti_vr.state, self.r_v.state)
            self.w_b_vr.update_hebb(dt, self.eff_b_vr.state, self.r_v.state)
            self.eff_ii_vr.upd(dt, inf_in)
            self.eff_eti_vr.upd(dt, envtch_in)
            self.eff_b_vr.upd(dt, self.w_b_vr.w)

        for i, net in enumerate(self.ssrmtr_nets):
            net.evolve(dt, inf_in, envtch_in[:, 2 * i : 2 * i + 1], vltmn_signal, vltmn_state, is_running_hebb)
        self.r_v.evolve(
            dt,
            signal_s_v + matmul(inf_in, self.w_ii_vr) + matmul(envtch_in, self.w_eti_vr) + self.w_b_vr.w,
            is_running_hebb
        )
        
    def forward(self):
        return torch.cat([net.forward() for net in self.ssrmtr_nets], axis = 1)
        
    def hebb_learn(self, dosage):
        self.w_ii_vr.hebb_learn(dosage)
        self.w_eti_vr.hebb_learn(dosage)
        self.w_b_vr.hebb_learn(dosage)
        self.r_v.hebb_learn(dosage)
        for net in self.ssrmtr_nets:
            net.hebb_learn(dosage)
        
    def hebb_panic(self, dt, rate):
        self.r_v.hebb_panic(
            dt,
            rate,
            sum([net.w_sr_vr.hebb_out() for net in self.ssrmtr_nets])
            + self.w_ii_vr.hebb_out() + self.w_eti_vr.hebb_out
            + self.w_b_vr.hebb_out()
        )
        for net in self.ssrmtr_nets:
            net.hebb_panic(dt, rate)
