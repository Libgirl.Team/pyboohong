import torch
from torch import nn
from torch import matmul
from torch import mul
from torch import randn
from torch.nn import Parameter
from PyBooHong.esn.components import BiasHbWts, InterHbWts, Reservoir, perturb, raise_infinite
from PyBooHong.esn.components import EfficacyConnections, EfficacyBias, oja_connections, oja_sources
from PyBooHong.util.utils import override_free_path
import logging
import os
import shutil
import tarfile

logger = logging.getLogger(__name__)

class ProprioceptorNet(nn.Module):
    def __init__(self,
                 n_pr, n_vr,
                 genw_ti_pr, genw_ei_pr, gen_eqlbrm_pr, genw_pr_vr, genw_vr_pr, genw_pr_o, genw_pr_pr,
                 sgm_panic, tau_hebb, gen_tau_pr, tau_vr,
                 fn_r, state_low_bnd, state_up_bnd, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd, rate_noise):
        super(ProprioceptorNet, self).__init__()
        self.r_p = Reservoir(n_r = n_pr,
                             gen_tau_dyn = gen_tau_pr,
                             fn_r = fn_r,
                             state_low_bnd = state_low_bnd,
                             state_up_bnd = state_up_bnd,
                             w_low_bnd = w_low_bnd,
                             w_up_bnd = w_up_bnd,
                             hb_low_bnd = hb_low_bnd,
                             hb_up_bnd = hb_up_bnd,
                             gen_w_intra = genw_pr_pr,
                             sgm_panic = sgm_panic,
                             tau_hebb = tau_hebb,
                             rate_noise = rate_noise)
        # ti: 1st: on/off, 2nd: tch signal.
        self.hbw_ti_pr = InterHbWts(2, n_pr, genw_ti_pr(torch.zeros(2, n_pr)),
                                    tau_hebb, oja_connections, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd)
        self.eff_ti_pr = EfficacyConnections(2, self.r_p.tau_dyn)
        self.hbw_ei_pr = InterHbWts(1, n_pr, genw_ei_pr(torch.zeros(1, n_pr)),
                                    tau_hebb, oja_connections, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd)
        self.eff_ei_pr = EfficacyConnections(1, self.r_p.tau_dyn)
        self.hbw_b_pr = BiasHbWts(n_pr, gen_eqlbrm_pr(self.r_p.tau_dyn) / self.r_p.tau_dyn,
                                  tau_hebb, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd)
        self.eff_b_pr = EfficacyBias(n_pr, self.r_p.tau_dyn)
        self.hbw_pr_vr = InterHbWts(n_pr, n_vr, genw_pr_vr(torch.zeros(n_pr, n_vr)),
                                    tau_hebb, oja_connections, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd)
        self.eff_pr_vr = EfficacyConnections(n_pr, tau_vr)
        self.hbw_vr_pr = InterHbWts(n_vr, n_pr, genw_vr_pr(torch.zeros(n_vr, n_pr)),
                                    tau_hebb, oja_connections, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd)
        self.eff_vr_pr = EfficacyConnections(n_vr, self.r_p.tau_dyn)
        self.hbw_pr_o = InterHbWts(n_pr, 1, genw_pr_o(torch.zeros(n_pr, 1)),
                                   tau_hebb, oja_sources, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd)
        self.hbw_pr_o.w.requires_grad = True
        # self.perturb_panic_o = Parameter(torch.zeros(1, 1))
        # self.state_panic_o = Parameter(torch.zeros(1, 1), requires_grad=False)
        
    def evolve(self, dt, tch_in, env_in, vltmn_in, vltmn_state, is_running_hebb):
        signal_pr = self.r_p.signal()
        signal_o = matmul(signal_pr, self.hbw_pr_o.w)
        if is_running_hebb:
            self.hbw_ti_pr.update_hebb(dt, self.eff_ti_pr.signal(), self.r_p.state)
            self.hbw_ei_pr.update_hebb(dt, self.eff_ei_pr.signal(), self.r_p.state)
            self.hbw_b_pr.update_hebb(dt, self.eff_b_pr.signal(), self.r_p.state)
            self.hbw_pr_vr.update_hebb(dt, self.eff_pr_vr.signal(), vltmn_state)
            self.hbw_vr_pr.update_hebb(dt, self.eff_vr_pr.signal(), self.r_p.state)
            self.hbw_pr_o.update_hebb(dt, signal_pr, signal_o)
            self.eff_ti_pr.upd(dt, tch_in)
            self.eff_ei_pr.upd(dt, env_in)
            self.eff_b_pr.upd(dt, self.hbw_b_pr.w)
            self.eff_pr_vr.upd(dt, signal_pr)
            self.eff_vr_pr.upd(dt, vltmn_in)
        self.r_p.evolve(dt,
                        self.hbw_b_pr.w + matmul(tch_in, self.hbw_ti_pr.w) + matmul(env_in, self.hbw_ei_pr.w)
                        + matmul(vltmn_in, self.hbw_vr_pr.w),
                        is_running_hebb)

    def signal_to_vltm(self):
        return matmul(self.r_p.signal(), self.hbw_pr_vr.w).detach()
        
    def forward(self):
        r_output = matmul(self.r_p.signal(), self.hbw_pr_o.w)
        return r_output
        # if is_tfb:
        #     return r_output + self.state_panic_o
        # else:
        #     return r_output
        
    def hebb_learn(self, dosage):
        # if is_instructive:
        #     self.hbw_pr_o.hebb_learn(dosage)
        self.hbw_ti_pr.hebb_learn(dosage)
        self.hbw_ei_pr.hebb_learn(dosage)
        self.hbw_b_pr.hebb_learn(dosage)
        self.hbw_pr_vr.hebb_learn(dosage)
        self.hbw_vr_pr.hebb_learn(dosage)
        self.r_p.hebb_learn(dosage)
        
    def hebb_panic(self, dt, rate_panic, rate_learn):
        # ## with decay for state_panic_o & perturb
        # if self.r_p.time_panic <= 0:
        #     self.perturb_panic_o = Parameter(perturb(self.perturb_panic_o, self.r_p.sgm_panic).detach())
        # self.state_panic_o.sub_(self.state_panic_o * rate_learn * dt
        #                         + dt * rate_panic * self.perturb_panic_o)
        # ## no decay for state_panic_o, no perturb
        # raise_infinite(self.state_panic_o, 'state_panic_o, before ProprioceptorNet.hebb_panic')
        # self.state_panic_o.sub_(err_acce * dt / tau_fit_o)
        # raise_infinite(self.state_panic_o, 'state_panic_o, after ProprioceptorNet.hebb_panic')
        self.r_p.hebb_panic(
            dt,
            rate_panic,
            # self.hbw_b_pr.hebb_out() + self.hbw_ti_pr.hebb_out()
            # + self.hbw_ei_pr.hebb_out() + self.hbw_vr_pr.hebb_out()
        )

    def grad_learn(self, dosage):
        if self.hbw_pr_o.w.grad:
            self.hbw_pr_o.w.add_(self.hbw_pr_o.w.grad * dosage).detach_()
            self.hbw_pr_o.w.zero_grad()

attrs_fname = 'attrs.pt'
pnet_format = 'ppcptr_net_{}.pt'
tartmp_format = '{}_tartmp'
            
class PVLTMN(nn.Module):
    def __init__(self,
                 n_ii, n_pr, n_vr, n_out,
                 genw_pr_pr, genw_ti_pr, genw_ei_pr, gen_eqlbrm_pr, genw_pr_vr, genw_vr_pr, genw_pr_o,
                 genw_vr_vr, genw_ti_vr, genw_ei_vr, genw_ii_vr, gen_eqlbrm_vr,
                 tau_hebb, sgm_panic, gen_tau_pr, gen_tau_vr,
                 fn_r, state_low_bnd, state_up_bnd, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd, rate_noise):
        super(PVLTMN, self).__init__()
        self.n_ii = n_ii
        self.n_out = n_out
        
        self.r_v = Reservoir(n_r = n_vr,
                             gen_tau_dyn = gen_tau_vr,
                             fn_r = fn_r,
                             state_low_bnd = state_low_bnd,
                             state_up_bnd = state_up_bnd,
                             w_low_bnd = w_low_bnd,
                             w_up_bnd = w_up_bnd,
                             hb_low_bnd = hb_low_bnd,
                             hb_up_bnd = hb_up_bnd,
                             gen_w_intra = genw_vr_vr,
                             sgm_panic = sgm_panic,
                             tau_hebb = tau_hebb,
                             rate_noise = rate_noise)

        self.ppcptr_nets = [ProprioceptorNet(
            n_pr = n_pr,
            n_vr = n_vr,
            genw_ti_pr = genw_ti_pr,
            genw_ei_pr = genw_ei_pr,
            gen_eqlbrm_pr = gen_eqlbrm_pr,
            genw_pr_vr = genw_pr_vr,
            genw_vr_pr = genw_vr_pr,
            genw_pr_o = genw_pr_o,
            genw_pr_pr = genw_pr_pr,
            sgm_panic = sgm_panic,
            tau_hebb = tau_hebb,
            gen_tau_pr = gen_tau_pr,
            tau_vr = self.r_v.tau_dyn,
            fn_r = fn_r,
            state_low_bnd = state_low_bnd,
            state_up_bnd = state_up_bnd,
            w_low_bnd = w_low_bnd,
            w_up_bnd = w_up_bnd,
            hb_low_bnd = hb_low_bnd,
            hb_up_bnd = hb_up_bnd,
            rate_noise = rate_noise
        ) for net in range(n_out)]

        # ti: o1[2], o2[2], o3[2]...
        self.hbw_ti_vr = InterHbWts(2 * n_out, n_vr, genw_ti_vr(torch.zeros(2 * n_out, n_vr)),
                                    tau_hebb, oja_connections, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd)
        self.eff_ti_vr = EfficacyConnections(2 * n_out, self.r_v.tau_dyn)
        self.hbw_ei_vr = InterHbWts(n_out, n_vr, genw_ei_vr(torch.zeros(n_out, n_vr)),
                                    tau_hebb, oja_connections, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd)
        self.eff_ei_vr = EfficacyConnections(n_out, self.r_v.tau_dyn)
        self.hbw_ii_vr = InterHbWts(n_ii, n_vr, genw_ii_vr(torch.zeros(n_ii, n_vr)),
                                    tau_hebb, oja_connections, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd)
        self.eff_ii_vr = EfficacyConnections(n_ii, self.r_v.tau_dyn)
        self.hbw_b_vr = BiasHbWts(n_vr, gen_eqlbrm_vr(self.r_v.tau_dyn) / self.r_v.tau_dyn,
                                  tau_hebb, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd)
        self.eff_b_vr = EfficacyBias(n_vr, self.r_v.tau_dyn)

    def evolve(self, dt, tch_in, env_in, inf_in, is_running_hebb, is_tfb):
        # all the inputs are normalized before injection.
        vltmn_signal = self.r_v.signal()
        vltmn_state = self.r_v.state
        signal_p_v = sum([net.signal_to_vltm() for net in self.ppcptr_nets])
        is_hebb_tfb = is_running_hebb & is_tfb
        
        if is_running_hebb:
            self.hbw_ei_vr.update_hebb(dt, self.eff_ei_vr.signal(), vltmn_state)
            self.hbw_ii_vr.update_hebb(dt, self.eff_ii_vr.signal(), vltmn_state)
            self.hbw_b_vr.update_hebb(dt, self.eff_b_vr.signal(), vltmn_state)
            self.eff_ei_vr.upd(dt, env_in)
            self.eff_ii_vr.upd(dt, inf_in)
            self.eff_b_vr.upd(dt, self.hbw_b_vr.w)

        if is_hebb_tfb:
            self.hbw_ti_vr.update_hebb(dt, self.eff_ti_vr.signal(), vltmn_state)
            self.eff_ti_vr.upd(dt, tch_in)

        for i, net in enumerate(self.ppcptr_nets): # how to run parallel?
            net.evolve(dt,
                       tch_in[:, 2 * i : 2 * (i + 1)],
                       env_in[:, i : i + 1],
                       vltmn_signal,
                       vltmn_state,
                       is_hebb_tfb)
            
        self.r_v.evolve(
            dt,
            signal_p_v + matmul(tch_in, self.hbw_ti_vr.w) + matmul(env_in, self.hbw_ei_vr.w)
            + matmul(inf_in, self.hbw_ii_vr.w) + self.hbw_b_vr.w,
            is_running_hebb
        )
        
    def forward(self):
        return torch.cat([net.forward() for net in self.ppcptr_nets], axis = 1)
        
    def hebb_learn(self, dosage, is_train_tfb):
        if is_train_tfb:
            for net in self.ppcptr_nets:
                net.hebb_learn(dosage)
            self.hbw_ti_vr.hebb_learn(dosage)
        self.hbw_ei_vr.hebb_learn(dosage)
        self.hbw_ii_vr.hebb_learn(dosage)
        self.hbw_b_vr.hebb_learn(dosage)
        self.r_v.hebb_learn(dosage)
        
    def hebb_panic(self, dt, rate_panic, rate_learn):
        self.r_v.hebb_panic(
            dt,
            rate_panic,
            # sum([net.hbw_pr_vr.hebb_out() for net in self.ppcptr_nets])
            # + self.hbw_ti_vr.hebb_out() + self.hbw_ei_vr.hebb_out() + self.hbw_ii_vr.hebb_out()
            # + self.hbw_b_vr.hebb_out()
        )
        for i, net in enumerate(self.ppcptr_nets):
            net.hebb_panic(dt, rate_panic, rate_learn,
                           #tau_fit_o, err_acce[i]
            )

    def grad_learn(self, dosage):
        for net in self.ppcptr_nets:
            net.grad_learn(dosage)

    def save(self, folder, prefix):
        tartmp_name = tartmp_format.format(prefix)
        tmp_path = override_free_path(os.path.join(folder, tartmp_name))
        tar_path = override_free_path(os.path.join(folder, '{}.tar'.format(prefix)))
        os.mkdir(tmp_path)
        
        with tarfile.open(tar_path, "w") as tar:
            torch.save(self.state_dict(), os.path.join(tmp_path, attrs_fname))
            for i, pn in enumerate(self.ppcptr_nets):
                torch.save(pn.state_dict(), os.path.join(tmp_path, pnet_format.format(i)))
            tar.add(tmp_path, arcname=tartmp_name)

        shutil.rmtree(tmp_path)

    def load(self, tar_path):
        head, tail = os.path.split(tar_path)
        
        with tarfile.open(tar_path) as tar:
            tar.extractall(head)
        
        prefix = tail[:-4]
        tmp_path = os.path.join(head, tartmp_format.format(prefix))
        attrs_path = os.path.join(tmp_path, attrs_fname)
        self.load_state_dict(torch.load(attrs_path))

        for i, pn in enumerate(self.ppcptr_nets):
            pn_path = os.path.join(tmp_path, pnet_format.format(i))
            pn.load_state_dict(torch.load(pn_path))

        shutil.rmtree(tmp_path)

    def to_device(self, device):
        self.to(device)
        for net in self.ppcptr_nets:
            net.to(device)

    def all_parameters(self):
        return list(self.parameters()) + sum([list(net.parameters()) for net in self.ppcptr_nets], [])
        
        
