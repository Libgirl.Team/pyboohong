from functools import reduce
import logging

logger = logging.getLogger(__name__)

def deep_equal(fn, location, tnsr1, tnsr2):
    truth = fn(tnsr1).equal(fn(tnsr2))
    if truth:
        logger.info('equal at {}.'.format(location))
    else:
        logger.error('unequal at {}.'.format(location))
    return truth

def equal_on_fns(fn_location_list, obj1, obj2):
    return reduce(lambda acc, x: deep_equal(x[0], x[1], obj1, obj2) and acc,
                  fn_location_list,
                  True)


equal_fns_hbwt = [(lambda hw: hw.w, 'HebbWeights.w'),
                  (lambda hw: hw.state_hebb, 'HebbWeights.state_hebb')]


def is_equal_hbwt(hw1, hw2, prefix):
    return equal_on_fns(map(lambda x: (x[0], prefix + x[1]), equal_fns_hbwt),
                        hw1, hw2)


equal_fns_effb = [(lambda effb: effb.state, 'EfficacyBias.state'),
                  (lambda effb: effb.post_tau, 'EfficacyBias.post_tau')]

def is_equal_effb(efb1, efb2, prefix):
    return equal_on_fns(map(lambda x: (x[0], prefix + x[1]), equal_fns_effb),
                        efb1, efb2)

equal_fns_effc = [(lambda effb: effb.state, 'EfficacyBias.state'),
                  (lambda effb: effb.w_tau, 'EfficacyBias.w_tau')]

def is_equal_effc(efc1, efc2, prefix):
    return equal_on_fns(map(lambda x: (x[0], prefix + x[1]), equal_fns_effc),
                        efc1, efc2)

equal_fns_rsvr = [(lambda rsvr: rsvr.tau_dyn, 'Reservoir.tau_dyn'),
                  (lambda rsvr: rsvr.tau_dyn_min, 'Reservoir.tau_dyn_min'),
                  (lambda rsvr: rsvr.state, 'Reservoir.state'),
                  (lambda rsvr: rsvr.perturbation_panic, 'Reservoir.perturbation_panic')]

def is_equal_rsvr(rsvr1, rsvr2, prefix):
    return for_all_relations(rsvr1, rsvr2,
                             [lambda r1, r2: equal_on_fns(map(lambda x: (x[0], prefix + x[1]),
                                                              equal_fns_rsvr),
                                                          r1, r2),
                              lambda r1, r2: is_equal_hbwt(r1.hbw_intra,
                                                           r2.hbw_intra,
                                                           prefix + 'Reservoir.hbw_intra.'),
                              lambda r1, r2: is_equal_effc(r1.eff_intra,
                                                           r2.eff_intra,
                                                           prefix + 'Reservoir.eff_intra.')])

def for_all_relations(x1, x2, relations):
    return reduce(lambda acc, rltn: rltn(x1, x2) and acc,
                  relations,
                  True)

def is_equal_ppcptr(n1, n2, prefix):
    return for_all_relations(
        n1,
        n2,
        [lambda x1, x2: is_equal_rsvr(x1.r_p, x2.r_p, prefix + 'ProprioceptorNet.r_p.'),
         lambda x1, x2: is_equal_hbwt(x1.hbw_ti_pr, x2.hbw_ti_pr, prefix + 'ProprioceptorNet.hbw_ti_pr.'),
         lambda x1, x2: is_equal_effc(x1.eff_ti_pr, x2.eff_ti_pr, prefix + 'ProprioceptorNet.eff_ti_pr.'),
         lambda x1, x2: is_equal_hbwt(x1.hbw_ei_pr, x2.hbw_ei_pr, prefix + 'ProprioceptorNet.hbw_ei_pr.'),
         lambda x1, x2: is_equal_effc(x1.eff_ei_pr, x2.eff_ei_pr, prefix + 'ProprioceptorNet.eff_ei_pr.'),
         lambda x1, x2: is_equal_hbwt(x1.hbw_b_pr, x2.hbw_b_pr, prefix + 'ProprioceptorNet.hbw_b_pr.'),
         lambda x1, x2: is_equal_effb(x1.eff_b_pr, x2.eff_b_pr, prefix + 'ProprioceptorNet.eff_b_pr.'),
         lambda x1, x2: is_equal_hbwt(x1.hbw_pr_vr, x2.hbw_pr_vr, prefix + 'ProprioceptorNet.hbw_pr_vr.'),
         lambda x1, x2: is_equal_effc(x1.eff_pr_vr, x2.eff_pr_vr, prefix + 'ProprioceptorNet.eff_pr_vr.'),
         lambda x1, x2: is_equal_hbwt(x1.hbw_vr_pr, x2.hbw_vr_pr, prefix + 'ProprioceptorNet.hbw_vr_pr.'),
         lambda x1, x2: is_equal_effc(x1.eff_vr_pr, x2.eff_vr_pr, prefix + 'ProprioceptorNet.eff_vr_pr.'),
         lambda x1, x2: is_equal_hbwt(x1.hbw_pr_o, x2.hbw_pr_o, prefix + 'ProprioceptorNet.hbw_pr_o.')]
    )

pvltmn_attrs = ['r_v',
                'hbw_ti_vr',
                'eff_ti_vr',
                'hbw_ei_vr',
                'eff_ei_vr',
                'hbw_ii_vr',
                'eff_ii_vr',
                'hbw_b_vr',
                'eff_b_vr']

def is_equal_pvltmn(n1, n2):
    if len(n1.ppcptr_nets) == len(n2.ppcptr_nets):
        if_eq_pnet = reduce(
            lambda acc, x: x and acc,
            map(lambda y: is_equal_ppcptr(y[1][0], y[1][1], 'PVLTMN.ppcptr_nets[{}]'.format(y[0])),
                enumerate(zip(n1.ppcptr_nets, n2.ppcptr_nets))),
            True
        )
    else:
        if_eq_pnet = False
    
    return for_all_relations(
        n1,
        n2,
        # [lambda x1, x2: is_equal_rsvr(getattr(x1, attr), getattr(x2, attr), 'PVLTMN.{}.'.format(attr))
        #  for attr in pvltmn_attrs] # is_equal should change! failed.
        ([lambda x1, x2: is_equal_rsvr(x1.r_v, x2.r_v, 'PVLTMN.r_v.'),
          lambda x1, x2: is_equal_hbwt(x1.hbw_ti_vr, x2.hbw_ti_vr, 'PVLTMN.hbw_ti_vr.'),
          lambda x1, x2: is_equal_effc(x1.eff_ti_vr, x2.eff_ti_vr, 'PVLTMN.eff_ti_vr.'),
          lambda x1, x2: is_equal_hbwt(x1.hbw_ei_vr, x2.hbw_ei_vr, 'PVLTMN.hbw_ei_vr.'),
          lambda x1, x2: is_equal_effc(x1.eff_ei_vr, x2.eff_ei_vr, 'PVLTMN.eff_ei_vr.'),
          lambda x1, x2: is_equal_hbwt(x1.hbw_ii_vr, x2.hbw_ii_vr, 'PVLTMN.hbw_ii_vr.'),
          lambda x1, x2: is_equal_effc(x1.eff_ii_vr, x2.eff_ii_vr, 'PVLTMN.eff_ii_vr.'),
          lambda x1, x2: is_equal_hbwt(x1.hbw_b_vr, x2.hbw_b_vr, 'PVLTMN.hbw_b_vr.'),
          lambda x1, x2: is_equal_effb(x1.eff_b_vr, x2.eff_b_vr, 'PVLTMN.eff_b_vr.')])
    ) and if_eq_pnet
