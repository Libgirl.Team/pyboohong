import torch
from torch import nn
from torch import matmul
from torch import mul
from torch import randn
from torch.nn import Parameter
import math
import random
from functools import reduce
import logging

logger = logging.getLogger(__name__)

def raise_infinite(tnsr, place):
    if torch.is_tensor(tnsr):
        if not torch.isfinite(tnsr).all().item():
            raise ValueError("Got infinite at {}.\nTensor:\n{}".format(place, tnsr))

def raise_infi_args(func):
    def inner(*args, **kwargs):
        for i, arg in enumerate(args):
            raise_infinite(arg, 'arg #{},{}'.format(i, func.__name__))
        for kw in kwargs:
            raise_infinite(kwargs[kw], '{}, {}'.format(kw, func.__name__))
        return func(*args, **kwargs)
    return inner
    
def cheap_tanh(x):
    a0 = 1.1
    a1 = 2.5
    b0 = 0.85
    return x.clamp(min=-a0, max=a0).mul(b0 / a0).add(
        x.abs().add(-a0).clamp(min=0.).mul((1 - b0) / a1 - a0).mul(x.sign())
    ).detach()

def steep_actv(x):
    d0 = 0.
    d1 = 0.05
    d2 = 0.10
    d3 = 0.50
    g1 = 0.43
    g2 = 0.05
    g3 = 0.02
    shifted_x = x - d0
    abs_shifted_x = shifted_x.abs().clamp(max=0.5)
    y1 = abs_shifted_x.clamp(max=d1) * g1 / d1
    y2 = (abs_shifted_x.clamp(max=d2) - d1).clamp(min=0.0) * g2 / (d2 - d1)
    y3 = (abs_shifted_x.clamp(max=d3) - d2).clamp(min=0.0) * g3 / (d3 - d2)
    return (0.5 + (y1 + y2 + y3) * shifted_x.sign()).detach()

# def gen_tch_in(state_env, tgt_env, c_tch):
#     return (tgt_env - state_env) / c_tch

# def get_loss(state_out_a, pre_u_tch, c_ins_free, c_tfb, c_tch, c_out):
#     filtered_tch = pre_u_tch.clamp(min=c_ins_free) + pre_u_tch.clamp(max=-c_ins_free)
#     fltr_sign = filtered_tch.sign()
#     filtered_tch = filtered_tch + fltr_sign * c_ins_free
#     fltr_abs = fltr_sign.abs()
#     tgt_z = filtered_tch * ( c_tfb * c_tch / c_out)
#     filtered_state_out_a = state_out_a * fltr_abs
#     criterion = torch.nn.MSELoss(reduction='sum')
#     return criterion(filtered_state_out_a, tgt_z)

@raise_infi_args
def oja_bias(pre, post, w):
    output = post * (pre - post * w)
    raise_infinite(output, 'output of oja_bias')
    return output.detach()

@raise_infi_args
def oja_sources(pre, post, w):
    output = matmul(pre.transpose(0, 1), post) - torch.mul(w, torch.square(post))
    raise_infinite(output, 'output of oja_sources')
    return output.detach()

@raise_infi_args
def oja_connections(pre, post, w):
    output = torch.mul(pre, post) - torch.mul(w, torch.square(post))
    raise_infinite(output, 'output of oja_connections')
    return output.detach()

class HebbWeights(nn.Module):
    def __init__(self, n_i, n_o, w, tau_hebb, low_bnd, up_bnd):
        super(HebbWeights, self).__init__()
        self.n_i = n_i
        self.n_o = n_o
        self.w = Parameter(w, requires_grad = False)
        self.state_hebb = Parameter(torch.zeros_like(self.w), requires_grad = False)
        self.tau_hebb = tau_hebb # for update_hebb
        self.low_bnd = low_bnd
        self.up_bnd = up_bnd
    def hebb_learn(self, dosage):
        raise_infinite(self.state_hebb, 'state_hebb, before HebbWeights.hebb_learn')
        self.w.add_(self.state_hebb * dosage).detach_()#.clamp_(self.low_bnd, self.up_bnd)
        raise_infinite(self.state_hebb, 'state_hebb, after HebbWeights.hebb_learn')
        
class BiasHbWts(HebbWeights):
    def __init__(self, n_o, w, tau_hebb, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd):
        super(BiasHbWts, self).__init__(1, n_o, w, tau_hebb, w_low_bnd, w_up_bnd)
        self.hb_low_bnd = hb_low_bnd
        self.hb_up_bnd = hb_up_bnd
    def update_hebb(self, dt, pre_eff, post):
        raise_infinite(self.state_hebb, 'state_hebb, BiasHbWts.update_hebb')
        raise_infinite(self.tau_hebb, 'tau_hebb, BiasHbWts.update_hebb')
        decay = - self.state_hebb / self.tau_hebb
        raise_infinite(decay, 'decay, BiasHbWts.update_hebb')
        (self.state_hebb.add_(dt * (decay + oja_bias(pre_eff, post, self.w))).detach_()
         #.clamp_(self.hb_low_bnd, self.hb_up_bnd)
        )
        raise_infinite(self.state_hebb, 'state_hebb, after BiasHbWts.update_hebb')
    # def hebb_out(self):
    #     return self.state_hebb.detach()

class InterHbWts(HebbWeights):
    def __init__(self, n_i, n_o, w, tau_hebb, fn_hebb, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd):
        super(InterHbWts, self).__init__(n_i, n_o, w, tau_hebb, w_low_bnd, w_up_bnd)
        self.hb_low_bnd = hb_low_bnd
        self.hb_up_bnd = hb_up_bnd
        self.fn_hebb = fn_hebb
    def update_hebb(self, dt, pre_eff, post):
        raise_infinite(self.state_hebb, 'state_hebb, InterHbWts.update_hebb')
        raise_infinite(self.tau_hebb, 'tau_hebb, InterHbWts.update_hebb')
        decay = - self.state_hebb / self.tau_hebb
        raise_infinite(decay, 'decay, InterHbWts.update_hebb')
        (self.state_hebb.add_(dt * (decay + self.fn_hebb(pre_eff, post, self.w))).detach_()
         #.clamp_(self.hb_low_bnd, self.hb_up_bnd)
        )
        raise_infinite(self.state_hebb, 'state_hebb, after InterHbWts.update_hebb')
    # def hebb_out(self, pre_eff):
    #     output = (matmul(pre_eff, self.state_hebb) / self.n_i).detach()
    #     raise_infinite(output, 'output of InterHbWts.hebb_out')
    #     return output

def gen_w_like(tnsr, mean, sgm, positive_rate, multi_in_factor):
    w0 = torch.randn_like(tnsr) * sgm / multi_in_factor + mean / multi_in_factor
    rnd_bool = torch.rand_like(tnsr).le(positive_rate)
    return (w0 * rnd_bool - w0 * ~ rnd_bool)

def gen_w(n_i, n_o, mean, sgm, positive_rate, multi_in_factor):
    w0 = torch.randn(n_i, n_o) * sgm / multi_in_factor + mean / multi_in_factor
    rnd_bool = torch.rand(n_i, n_o).le(positive_rate)
    return w0 * rnd_bool - w0 * ~ rnd_bool

class EfficacyBias(nn.Module):
    def __init__(self, n, post_tau):
        super(EfficacyBias, self).__init__()
        self.state = Parameter(torch.zeros(1, n), requires_grad = False)
        self.post_tau = Parameter(post_tau, requires_grad = False)
    def upd(self, dt, signal):
        raise_infinite(signal, 'signal, EfficacyBias.upd')
        raise_infinite(self.state, 'state, EfficacyBias.upd')
        raise_infinite(self.post_tau, 'post_tau, EfficacyBias.upd')
        decay = - self.state / self.post_tau
        raise_infinite(decay, 'decay, EfficacyBias.upd')
        self.state.add_(dt * (decay + signal)).detach_()
        raise_infinite(self.state, 'state, after EfficacyBias.upd')
    def signal(self):
        return self.state / self.post_tau

class EfficacyConnections(nn.Module):
    def __init__(self, n, post_tau):
        super(EfficacyConnections, self).__init__()
        self.n_pre = n
        self.n_post = post_tau.size()[1]
        self.state = Parameter(torch.zeros(n, self.n_post), requires_grad = False)
        #post_tau dim: [1, n_post]
        self.w_tau = Parameter(torch.cat(([post_tau for i in range(self.n_pre)]), axis=0),
                               requires_grad = False)
    def upd(self, dt, signal):
        raise_infinite(signal, 'signal, EfficacyConnections.upd')
        raise_infinite(self.state, 'state, EfficacyConnections.upd')
        raise_infinite(self.w_tau, 'w_tau, EfficacyConnections.upd')
        decay = - self.state / self.w_tau
        raise_infinite(decay, 'decay, EfficacyConnections.upd')
        (self.state.add_(dt * (decay
                              + torch.cat(([signal for i in range(self.n_post)]), axis=0).transpose(0, 1)))
         .detach_())
        raise_infinite(self.state, 'state, after EfficacyConnections.upd')
    def signal(self):
        return self.state / self.w_tau

def perturbed(tnsr, sigma):
    return tnsr + torch.randn_like(tnsr) * sigma

def perturb(tnsr, sigma):
    return (torch.randn_like(tnsr) * sigma + 1).detach()
    
class Reservoir(nn.Module):
    def __init__(self, n_r, gen_tau_dyn, fn_r, state_low_bnd, state_up_bnd,
                 w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd,
                 gen_w_intra, sgm_panic, tau_hebb, rate_noise):
        super(Reservoir, self).__init__()
        self.n_r = n_r
        self.tau_dyn = Parameter(gen_tau_dyn(torch.zeros(1, n_r)), requires_grad=False)
        self.tau_dyn_min = Parameter(self.tau_dyn.min(), requires_grad=False)
        self.fn_r = fn_r
        self.up_bnd = state_up_bnd
        self.low_bnd = state_low_bnd
        self.state = Parameter(
            torch.rand(1, self.n_r) * (self.up_bnd - self.low_bnd) + self.low_bnd,
            requires_grad=False
        ) # rand init state
        self.hbw_intra = InterHbWts(n_r, n_r, gen_w_intra(torch.zeros(n_r, n_r)),
                                    tau_hebb, oja_connections, w_low_bnd, w_up_bnd, hb_low_bnd, hb_up_bnd)
        self.eff_intra = EfficacyConnections(n_r, self.tau_dyn)
        self.sgm_panic = sgm_panic
        self.time_panic = 0.0
        self.perturbation_panic = Parameter(torch.zeros(1, self.n_r), requires_grad=False) # for panic
        self.rate_noise = rate_noise

    def evolve(self, dt, inj, is_running_hebb):
        raise_infinite(inj, 'inj, Reservoir.evolve')
        raise_infinite(self.state, 'state, Reservoir.evolve')
        raise_infinite(self.tau_dyn, 'tau_dyn, Reservoir.evolve')
        if is_running_hebb:
            self.hbw_intra.update_hebb(dt, self.eff_intra.signal(), self.state)
            self.eff_intra.upd(dt, inj)
        rate = - self.state / self.tau_dyn + inj
        raise_infinite(rate, 'rate, Reservoir.evolve')
        self.state_add(rate * dt)
        self.state_perturb(dt)
        raise_infinite(self.state, 'state, after state_perturb of Reservoir.evolve')

    def signal(self):
        return self.fn_r(self.state)

    def hebb_learn(self, dosage):
        self.hbw_intra.hebb_learn(dosage)

    def state_add(self, delta):
        raise_infinite(delta, 'delta, Reservoir.state_add')
        self.state.add_(delta).clamp_(min=self.low_bnd, max=self.up_bnd).detach_()
        raise_infinite(self.state, 'state, after Reservoir.state_add')

    def state_perturb(self, dt):
        perturbation = torch.rand_like(self.state)
        prob = math.exp(- self.rate_noise * dt) * self.rate_noise * dt # Poisson
        (self.state
         .mul_(perturbation.ge(prob))
         .add_((self.low_bnd + (self.up_bnd - self.low_bnd) * torch.rand_like(self.state))
               * perturbation.lt(prob))
         .detach_())
        raise_infinite(self.state, 'state, after Reservoir.state_perturb')
        
    def hebb_panic(self, dt, rate):
        #raise_infinite(inj, 'inj, Reservoir.hebb_panic')
        if self.time_panic <= 0: # not let every node has its own time_panic for efficiency
            self.time_panic = self.tau_dyn_min / 8.0 * random.uniform(0.3, 1.7)
            self.perturbation_panic.data = perturb(self.perturbation_panic, self.sgm_panic)
            raise_infinite(self.perturbation_panic, 'perturbation_panic, Reservoir.hebb_panic')

        #hebb_out_intra = self.hbw_intra.hebb_out()
        raise_infinite(self.perturbation_panic, 'perturbation_panic, Reservoir.hebb_panic before state_add')
        #raise_infinite(hebb_out_intra, 'hebb_out_intra, Reservoir.hebb_panic before state_add')
        self.state_add(
            - rate * dt * self.perturbation_panic * self.state
        )
        self.time_panic = self.time_panic - dt


def gen_tau_dyn(tnsr, ls): # ls: [((tau_low_bnd, tau_up_bnd), ratio)]
    ls_bnds, _ = reduce(lambda acc, x: (acc[0] + [(x[0], (acc[1], acc[1] + x[1]))],
                                        acc[1] + x[1]),
                        map(lambda x: (x[0], x[1] / sum(map(lambda x: x[1], ls))),
                            ls),
                        ([],0))
    prob = torch.rand_like(tnsr)
    return sum(map(lambda x: ((prob.ge(x[1][0]) & prob.lt(x[1][1]))
                              * (x[0][0] + (x[0][1] - x[0][0]) * torch.rand_like(tnsr))),
                   ls_bnds))    
