gamma = 1.0
beta = 2.0
tau = 2.0
n = 9.65
n_per_tau = 20
dt = tau / n_per_tau

def v_mackey_glass(x0, x1):
    return beta * x0 / (1 + x0 ** 9.65) - gamma * x1

def mackey_glass_seq(max_n):
    seq = []
    for i in range(- n_per_tau, max_n):
        t = i * dt
        if i <= 0:
            seq.append((i, t, 0.5))
        else:
            last_n = len(seq)-1
            pre_n = last_n - n_per_tau
            x0 = seq[pre_n][2]
            x1 = seq[last_n][2]
            new_x = x1 + dt * v_mackey_glass(x0, x1)
            seq.append((i, t, new_x))
    return seq


seq = mackey_glass_seq(100000)
seq_df = pd.DataFrame(seq, columns = ['step', 't', 'x'])
seq_df.to_csv('mackey-glass-00.csv')

v_seq_map = map(lambda z0, z1: (z1[0], z1[1], (z1[2] - z0[2])/dt ) , seq[:-1] , seq[1:])
v_seq = list(v_seq_map)
v_seq_df = pd.DataFrame(v_seq, columns = ['step', 't', 'x'])
v_seq_df.to_csv('mackey-glass-v-00.csv')

a_seq_map = map(lambda z0, z1: (z1[0], z1[1], (z1[2] - z0[2])/dt ) , v_seq[:-1] , v_seq[1:])
a_seq = list(a_seq_map)
a_seq_df = pd.DataFrame(a_seq, columns = ['step', 't', 'x'])
a_seq_df.to_csv('mackey-glass-a-00.csv')

