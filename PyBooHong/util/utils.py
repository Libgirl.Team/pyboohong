import os.path
import logging

logger = logging.getLogger(__name__)

def override_free_path(path):
    head, tail = os.path.split(path)
    logger.debug('path head: {}, tail: {}.'.format(head, tail))
    if not (head and os.path.isdir(head)):
        logger.error('got path error; try to use the working directory \'.\'')
        head = '.'
    path = os.path.join(head, tail)
    # path = os.path.join(head if head and os.path.isdir(head) else '.',
    #                     tail)
    if os.path.isfile(path) or os.path.isdir(path):
        path = os.path.join(head, '_' + tail)
        return override_free_path(path)
    else:
        return path

def pd_df_to_csv(df, path):
    """
    df is pandas.DataFrame
    """
    if os.path.isfile(path):
        head, tail = os.path.split(path)
        path = os.path.join(head,'_' + tail)
    df.to_csv(path)

def gen_extensible_obj():
    return type('MyObject', (object,), {})()
