import torch
import random
import time
from functools import reduce
import pandas as pd
import numpy as np
import logging
from PyBooHong.util.utils import override_free_path
import os.path

logger = logging.getLogger(__name__)

def get_loss(nacce, pre_tch):
    # normalized acce: [[o1, o2, ..]], tch: [[o1s, o1v, o2s, o2v, ..]]. s=swtich. v = value.
    criterion = torch.nn.MSELoss(reduction='sum')
    tch_switch = torch.zeros_like(nacce)
    tch_value = torch.zeros_like(nacce)
    for i in range(nacce.size()[1]):
        tch_switch[0, i] = pre_tch[0, 2 * i]
        tch_value[0, i] = pre_tch[0, 2 * i + 1]
    nacce = nacce * tch_switch
    tch_value = tch_value * tch_switch
    return criterion(nacce, tch_value)

class TchGen():
    def __init__(self, on_prob, rate):
        self.on_prob = on_prob
        self.rate = rate
        self.new_t()
    def new_t(self):
        self.t = random.uniform(300., 700.)
        self.is_on = random.random() < self.on_prob
    def gen(self, dt, npos, tgt_npos):
        n_out = npos.size()[1]
        tch = torch.zeros(1, 2 * n_out)
        if self.t <= 0:
            self.new_t()
            
        if self.is_on:
            tch_value = (npos - tgt_npos) * self.rate
            for i in range(n_out):
                tch[0, 2 * i] = 1
                tch[0, 2 * i + 1] = tch_value[0, i]
        else:
            for i in range(n_out):
                tch[0, 2 * i] = 0
                tch[0, 2 * i + 1] = random.uniform(-1, 1)
            
        self.t = self.t - dt
        return tch.detach()

class RndIIGen():
    def __init__(self, md):
        self.n_ii = md.n_ii
        self.tau = min([net.r_p.tau_dyn.min() for net in md.ppcptr_nets] + [md.r_v.tau_dyn.min()]) / 5.
        self.t = self.tau * random.uniform(0.6, 1.4)
        self.inf_in = torch.randn(1, self.n_ii)
    def gen(self, dt):
        if self.t <= 0:
            self.t = self.tau * random.uniform(0.6, 1.4)
            self.inf_in = torch.randn(1, self.n_ii)
        self.t -= dt
        return self.inf_in

def genw_bias_forgetting(tnsr_tau, sgm):
    return torch.randn_like(tnsr_tau) * sgm / tnsr_tau

def genw_connection_forgetting(tnsr, sgm, multi_in_factor):
    return torch.randn_like(tnsr) * sgm / multi_in_factor

def genw_connection_memorizing(tnsr, sgm, pos_rate, multi_in_factor):
    rnd_bool = torch.rand_like(tnsr).lt(pos_rate)
    w_abs = torch.randn_like(tnsr).abs()
    w_pos = w_abs * rnd_bool
    w_neg = - w_abs * ~ rnd_bool
    return (w_pos + w_neg) * sgm / multi_in_factor

def gen_equil_forgetting(tnsr_tau, mean, sgm): # for vltm, memorizing.
    return torch.randn_like(tnsr_tau) * sgm + mean

def gen_equil_memorizing(tnsr_tau, on_rate, low, high, sgm): # for vltm, memorizing.
    w0 = torch.randn_like(tnsr_tau) * sgm + low
    w1 = torch.randn_like(tnsr_tau) * sgm + high
    rnd_bool = torch.rand_like(tnsr_tau).lt(on_rate)
    return w1 * rnd_bool + w0 * ~ rnd_bool

def n_suffix(n, ls):
    return reduce(lambda acc, x: acc + x,
                  map(lambda txt: ['{}_{}'.format(txt, i) for i in range(n)], ls),
                  [])

def tch_separate(tch, idx, name):
    if tch.size()[1] % 2 != 0:
        raise ValueError("tch_{} got tch of odd size {}. tch:\n{}".format(name, tch.size(), tch))
    size = tch.size()[1] // 2
    output = torch.zeros(1, size)
    for n in range(size):
        output[:, n] = tch[:, 2 * n + idx]
    return output

get_tch_switch = lambda tch: tch_separate(tch, 0, 'swtich')
get_tch_value = lambda tch: tch_separate(tch, 1, 'value')

def run_n_save(md_df_gen, md, epochs, store_path=None, md_prefix=None, df_prefix=None):
    if not store_path and (md_prefix or df_prefix):
        logger.error('Got no store_path for saving!')
    else:
        for ep in range(epochs):
            logger.info('======== epoch {} ========'.format(ep))
            md, df = md_df_gen(md)
            if md_prefix:
                md.save(os.path.join(store_path,'models'), '{}_ep{}'.format(md_prefix, ep)) 
            if df_prefix:
                df.to_csv(override_free_path(os.path.join(store_path,
                                                          'inferences', '{}_ep{}.csv'.format(df_prefix, ep))))

def run_eval(md, dt, n_steps, tnsr_z_seq_1out, is_train_tfb, base_rate_panic, rate_learn, optm_fn):
    tnsr_z_seq = torch.cat([tnsr_z_seq_1out for i in range(md.n_out)], dim=2)
    # if not n_steps:
    #     n_steps = tnsr_z_seq_1out['y'].size
    start_time = time.time()
    columns = (['step', 't', 'loss']
               + n_suffix(md.n_out,
                          ['tgt_npos', 'npos', 'nvelo', 'tch_switch', 'tch_value', 'nacce',
                           'state_hebb_pr_vr_max', 'w_pr_vr_max', 'oja_pr_vr_max',
                           'state_hebb_pr_vr_min', 'w_pr_vr_min', 'oja_pr_vr_min',
                          # 'state_panic_o'
                          ]))
    hist_df = pd.DataFrame(np.zeros((n_steps, len(columns))), columns=columns)    
    loss_sum = 0
    steps_w_loss = 0
    loss = 0
    current_t = 0
    rnd_ii_gen = RndIIGen(md)
    ti_gen = TchGen(0.5, 1.0)
    nvelo = md()
    nvelo_m1 = nvelo
    npos = torch.zeros(1, md.n_out)
    tch = ti_gen.gen(dt, npos, tnsr_z_seq[0])
    tch_m1 = tch
    optmz = optm_fn(md.all_parameters(), rate_learn)
    for i in range(0, n_steps):
        # # for state_panic_o
        # if loss > 0:
        #     nvelo = md(True)
        # else:
        #     nvelo = md()
        nvelo = md()
        nacce = (nvelo - nvelo_m1) / dt
        tgt_npos = tnsr_z_seq[i]
        tch_m1 = tch
        tch = ti_gen.gen(dt, npos, tgt_npos)
        optmz.zero_grad()
        loss = get_loss(nacce, tch_m1)
        loss.backward()
        
        if loss > 0:
            loss_sum += loss.detach().numpy().item()
            steps_w_loss += 1
        elif loss < 0:
            logger.error('Err: loss = {} < 0.'.format(loss))
        
        hist_df['step'][i] = i
        hist_df['t'][i] = current_t
        hist_df['loss'][i] = loss.detach().numpy().item()

        tch_switch = get_tch_switch(tch)
        tch_value = get_tch_value(tch)
        for n in range(md.n_out):
            hist_df['tgt_npos_{}'.format(n)][i] = tgt_npos[0, n].numpy().item()
            hist_df['npos_{}'.format(n)][i] = npos[0, n].detach().numpy().item()
            hist_df['nvelo_{}'.format(n)][i] = nvelo[0, n].detach().numpy().item()
            hist_df['tch_switch_{}'.format(n)][i] = tch_switch[0, n].detach().numpy().item()
            hist_df['tch_value_{}'.format(n)][i] = tch_value[0, n].detach().numpy().item()
            hist_df['nacce_{}'.format(n)][i] = nacce[0, n].detach().numpy().item()
            # for checking divergence.
            net_n = md.ppcptr_nets[n]
            #hist_df['state_panic_o_{}'.format(n)][i] = (net_n.state_panic_o)
            hist_df['state_hebb_pr_vr_max_{}'.format(n)][i] = (net_n.hbw_pr_vr.state_hebb.max())
            hist_df['state_hebb_pr_vr_min_{}'.format(n)][i] = (net_n.hbw_pr_vr.state_hebb.min())
            hist_df['w_pr_vr_max_{}'.format(n)][i] = (net_n.hbw_pr_vr.w.max())
            hist_df['w_pr_vr_min_{}'.format(n)][i] = (net_n.hbw_pr_vr.w.min())
            hist_df['oja_pr_vr_max_{}'.format(n)][i] = (
                net_n.hbw_pr_vr.fn_hebb(net_n.eff_pr_vr.signal(), md.r_v.state, net_n.hbw_pr_vr.w).max()
            )
            hist_df['oja_pr_vr_min_{}'.format(n)][i] = (
                net_n.hbw_pr_vr.fn_hebb(net_n.eff_pr_vr.signal(), md.r_v.state, net_n.hbw_pr_vr.w).min()
            )

        nvelo_m1 = nvelo.detach()
        npos.add_(nvelo * dt).clamp_(-1., 1.).detach_()
        md.evolve(dt, tch, npos, rnd_ii_gen.gen(dt), True, is_train_tfb)

        if is_train_tfb:
            md.hebb_panic(dt,
                          base_rate_panic * loss.sqrt(),
                          rate_learn,
                          # tau_fit_o, get_tch_value(tch_m1) - nacce
            )
        
        md.hebb_learn(dt * rate_learn, is_train_tfb) # always on!!!
        if ti_gen.is_on:
            optmz.step()
            #md.grad_learn(dt * rate_learn)
        current_t += dt
    
    if steps_w_loss == 0:
        loss_avg = 0
    else:
        loss_avg = loss_sum / steps_w_loss

    logger.info('loss_avg: {}, steps_w_loss: {}, total steps: {}, dt: {}'.format(
        loss_avg, steps_w_loss, n_steps, dt
    ))

    return md, hist_df
    
