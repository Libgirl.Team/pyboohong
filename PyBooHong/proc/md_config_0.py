import PyBooHong.esn.esn as esn
import os.path
import pandas as pd
import torch

dt = 0.1

n_tch_in = 1
n_inf_in = 1
n_rsvr = 3
n_out = 1
tau_rsvr = 1.0e+0
tau_out = 1.5e+0
fn_rsvr = esn.cheap_tanh
acce_rate = 2.0
c_tfb = 1.0
c_out = 2.0
#c_out = acce_rate / tau_out
c_ins_free = 0.02

c_tch = 1.0e+1
gen_tch_in = lambda state_out, tgt_out: esn.gen_tch_in(state_out, tgt_out, c_tch)

lr=1e-2/n_out

## path
store_path = '/Users/baliuzeger/projects/imitation_learning/PyBooHong/store'

## Prepare train data
train_data_path = os.path.join(store_path, 'datasets/y_seq/y-tfb-500x-train-00.csv')
train_data_df = pd.read_csv(train_data_path)
tnsr_train_in_seq = torch.tensor(list(map(lambda x: (x,), train_data_df['y'])))
total_steps = tnsr_train_in_seq.size()[0]
