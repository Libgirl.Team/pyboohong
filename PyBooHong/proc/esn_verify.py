from PyBooHong.esn.esn import *

def verify_save_load():
    n_tch_in = 2
    n_inf_in = 3
    n_rsvr = 5
    n_out = 1
    tau_rsvr = 10.0
    tau_out = 10.0
    fn_rsvr = cheap_tanh
    acce_rate = 2.0

    save_path = 'echo_imitator_tmp1.pt'
    
    md = EchoImitator(n_tch_in=n_tch_in,
                      n_inf_in=n_inf_in,
                      n_rsvr=n_rsvr,
                      n_out=n_out,
                      tau_rsvr=tau_rsvr,
                      tau_out=tau_out,
                      fn_rsvr = cheap_tanh,
                      acce_rate=acce_rate)

    torch.save(md.state_dict(), save_path)

    state_env_0 = torch.zeros(n_out)
    tch_in_0 = torch.ones(n_tch_in)
    inf_in_0 = torch.ones(n_inf_in)
    dt = 0.1

    state_env_a = state_env_0.clone().detach()
    state_env_a = md(dt, tch_in_0, inf_in_0, state_env_a)

    md_b = EchoImitator(n_tch_in=n_tch_in,
                      n_inf_in=n_inf_in,
                      n_rsvr=n_rsvr,
                      n_out=n_out,
                      tau_rsvr=tau_rsvr,
                      tau_out=tau_out,
                      fn_rsvr = cheap_tanh,
                      acce_rate=acce_rate)
    loaded = torch.load(save_path)
    md_b.load_state_dict(loaded)
    state_env_b = state_env_0.clone().detach()
    state_env_b = md_b(dt, tch_in_0, inf_in_0, state_env_b)

    print(state_env_a  - state_env_b)


def get_loss(state_out, pre_u_tch):
    filtered_tch = pre_u_tch.clamp(min=0.05) + pre_u_tch.clamp(max=-0.05)
    fltr_sign = filtered_tch.sign()
    fltr_abs = fltr_sign.abs()
    tgt_z = filtered_tch * ( c_tfb * c_tch / md.c_out)
    filtered_state_out = state_out * fltr_abs
    criterion = torch.nn.MSELoss(reduction='sum')
    return criterion(filtered_state_out, tgt_z)
    
def verify_tfb_training():
    n_tch_in = 1
    n_inf_in = 1
    n_rsvr = 100
    n_out = 1
    tau_rsvr = 10.0
    tau_out = 10.0
    fn_rsvr = cheap_tanh
    acce_rate = 2.0
    dt = 0.1
    c_tfb = 1.0
    py_path = '/Users/baliuzeger/projects/imitation_learning/PyBooHong'
    md_save_path = os.path.join(il_path, 'PyBooHong/store/models/echo-imitator-try-fit-last.pt')

    md = esn.EchoImitator(n_tch_in=n_tch_in,
                          n_inf_in=n_inf_in,
                          n_rsvr=n_rsvr,
                          n_out=n_out,
                          tau_rsvr=tau_rsvr,
                          tau_out=tau_out,
                          fn_rsvr = cheap_tanh,
                          acce_rate=acce_rate)


    train_data_path = os.path.join(py_path,'store/datasets/y_seq/y-tfb-500x-train-00.csv')
    train_data_df = pd.read_csv(train_data_path)
    tnsr_train_in_seq = torch.tensor(list(map(lambda x: (x,), train_data_df['y'])))
    total_steps = tnsr_train_in_seq.size()[0]

    c_tch = 1.0
    gen_tch_in = lambda state_out, tgt_out: esn.gen_tch_in(state_out, tgt_out, c_tch)

    optimizer = torch.optim.SGD(md.parameters(), lr=1e-2, momentum=0.9)

    r_to_0_init = md.r_to_o.copy().detach()

    state_env = torch.zeros(n_out)
    inf_in_0 = torch.zeros(1)

    optimizer.zero_grad()
    for i in range(0, total_steps):
        u_tch= gen_tch_in(state_env, tnsr_train_in_seq[i])
        state_env, state_out = md(dt, u_tch, inf_in_0, state_env)
        state_env = state_env.detach()
        loss = get_loss(state_out, u_tch)
        loss.backward()






        

