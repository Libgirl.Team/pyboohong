from PyBooHong.scripts.md_config_0 import *
from PyBooHong.util.utils import *

def make_new_md(name):
    md_save_path = os.path.join(store_path, 'models/{}.pt'.format(name))
    md = esn.EchoImitator(n_tch_in=n_tch_in,
                          n_inf_in=n_inf_in,
                          n_rsvr=n_rsvr,
                          n_out=n_out,
                          tau_rsvr=tau_rsvr,
                          tau_out=tau_out,
                          fn_rsvr = fn_rsvr)
    torch.save(md.state_dict(), override_free_path(md_save_path))
    return md

def run(md, cus_total_steps = None, save_md_name = None, save_plot_name = None):
    if save_md_name or save_plot_name:
        if save_plot_name:
            plot_save_path = override_free_path(os.path.join(
                store_path, 'material/{}.csv'.format(save_plot_name)
            ))
        if save_md_name:
            md_save_path = os.path.join(store_path, 'models/{}.pt'.format(save_md_name))
            pre_tgt_env_m2 = None
            pre_tgt_env_m1 = None
            u_tch_m1 = None
            #optimizer = torch.optim.SGD(md.parameters(), lr=1e-3/n_rsvr, momentum=0.9)
            #optimizer.zero_grad()
        if cus_total_steps:
            total_steps = cus_total_steps
        history = []
        state_env = torch.zeros(n_out)
        state_out_a = torch.zeros(n_out)
        inf_in_0 = torch.zeros(1) # should make noise!!
        for i in range(0, total_steps):
            tgt_env = tnsr_train_in_seq[i]
            tgt_env_value = tgt_env.numpy().item()
            u_tch= gen_tch_in(state_env, tgt_env)
            if save_plot_name:
                state_env_value = state_env.numpy().item()
                err_value = state_env_value - tgt_env_value
                u_tch_value = u_tch.numpy().item()
                state_out_a_value = state_out_a.detach().numpy().item()
                history.append((
                    i, i * dt, tgt_env_value, state_env_value, err_value, u_tch_value, state_out_a_value
                ))
            state_env, state_out_a = md(dt, u_tch, inf_in_0, state_env / c_out)
            state_env = state_env * c_out
            state_env = state_env.clamp(min=-2.0, max=2.0).detach()
            if save_md_name:
                if pre_tgt_env_m2 == pre_tgt_env_m1 == tgt_env_value:
                    #print(pre_tgt_env_m2 == pre_tgt_env_m1 == tgt_env_value)
                    loss = get_loss(state_out_a, u_tch_m1)
                    loss.backward()
                    md.r_to_o = torch.nn.Parameter(md.r_to_o + md.r_to_o.grad.mul(-lr))
                    #optimizer.step()
                pre_tgt_env_m2 = pre_tgt_env_m1
                pre_tgt_env_m1 = tgt_env_value
                u_tch_m1 = u_tch
            
        if save_plot_name:
            df = pd.DataFrame(history, columns = [
                'step', 'time', 'tgt_env', 'state_env', 'err', 'tch', 'state_out_a'
            ])
            df.to_csv(plot_save_path)

        if save_md_name:
            #optimizer.step()
            torch.save(md.state_dict(), override_free_path(md_save_path))

        return md
