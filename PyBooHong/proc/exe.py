import logging

logger = logging.getLogger(__name__)

def exe(proc):
    # global cntr # should be left by some trials for testing using outter variables?
    logger.info('\n\n======================= Process start =======================')
    try:
        proc()
    except Exception as e:
        logger.exception(e)
    logger.info('Process end.')

def trial_proc():
    # global cntr # should be left by some trials for testing using outter variables?
    logger.info('test info in trial_proc.')
    logger.debug('debug msg!')
    raise ValueError('testing value exception!')
