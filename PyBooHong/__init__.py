import yaml
import logging
import logging.config
import os
import git

init_head, init_tail = os.path.split(os.path.abspath(__file__))
log_conf_path = os.path.join(init_head, 'logging_conf.yaml')

with open(log_conf_path) as log_conf_file:
    log_conf_dict = yaml.load(log_conf_file, Loader=yaml.FullLoader)
    logging.config.dictConfig(log_conf_dict)

logger = logging.getLogger(__name__)

try:
    repo = git.Repo(search_parent_directories=True)
    logger.info('\n\n======================= Load PyMooHong =======================\ngit sha: {}\n'
                 .format(repo.head.object.hexsha))
    modified_diffs = repo.index.diff(None)
    if modified_diffs:
        logger.warning('Some files are modified and not yet committed!')
except Exception as e:
    logger.error('Got exception when getting get sha:')
    logger.exception(e)
    
