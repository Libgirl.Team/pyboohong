import math
import random
import pandas as pd
import PyBooHong.util.utils as util
import os.path

min_t = 50.0
max_t = 200.0
total_t = max_t * 50

def rnd_period():
    return random.uniform(min_t, max_t)

def rnd_y():
    return random.uniform(-2.0, 2.0)

def gen_y_segments(total_t):
    seq = []
    t = 0.
    while t < total_t:
        period = rnd_period()
        t += period
        seq.append((t, rnd_y()))
    return pd.DataFrame(seq, columns=['End_t', 'y'])

def exe_gen_segments(il_path):
    # il_path = '/Users/baliuzeger/projects/imitation_learning'
        for setname in ['train', 'test']:
            seg1_df = gen_y_segments(total_t)
            path = os.path.join(il_path,
                                'PyBooHong/store/datasets/y_seq/y-segments-50x-{n}-00.csv'.format(n = setname))
            print(path)
            util.pd_df_to_csv(seg1_df, path)

def gen_y_seq(dt, y_seg_df):
    seq = []
    t = 0.
    for index, row in y_seg_df.iterrows():
        while t < row['End_t']:
            seq.append((t, row['y']))
            t += dt
    return pd.DataFrame(seq, columns=['t', 'y'])
