import math
import random
import pandas as pd
from PyBooHong.util.utils import *
import os.path

dt = 0.1
min_period_steps = 500
max_period_steps = 2000
total_steps = max_period_steps * 50

def rnd_period():
    return random.randint(min_period_steps, max_period_steps)

def rnd_y():
    return random.uniform(-2.0, 2.0)

def gen_y_seq(max_n):
    seq = []
    tmp_cnt = rnd_period()
    y = rnd_y()

    for i in range(0, max_n):
        t = i * dt
        seq.append((t, y))
        
        if tmp_cnt <= 0:
            tmp_cnt = rnd_period()
            y = rnd_y()
        else:
            tmp_cnt -= 1
    return seq
    
def execute():
    for setname in ['train', 'test']:
        y_seq = gen_y_seq(total_steps)
        df = pd.DataFrame(y_seq, columns=['t', 'y'])
        il_path = '/Users/baliuzeger/projects/imitation_learning'
        path = os.path.join(il_path,
                            'PyBooHong/store/datasets/y_seq/y-tfb-500x-{n}-00.csv'.format(n = setname))
        print(path)
        pd_df_to_csv(df, path)

execute()
