import math
import random
import pandas as pd
from util.utils import pd_df_to_csv

dt = 0.1
min_period_steps = 500
max_period_steps = 2000
total_steps = max_period_steps * 1000

def task_fn(x):
    return math.sin(5 * x + 0.5)

def rnd_period():
    return random.randint(min_period_steps, max_period_steps)

def rnd_x():
    return random.uniform(-10.0, 10.0)

def gen_sin_seq(max_n):
    seq = []
    tmp_cnt = rnd_period()
    x = rnd_x()

    for i in range(0, max_n):
        t = i * dt
        seq.append((t, x, task_fn(x)))
        
        if tmp_cnt <= 0:
            tmp_cnt = rnd_period()
            x = rnd_x()
        else:
            tmp_cnt -= 1
    return seq

def execute_sin():
    train_seq = gen_sin_seq(total_steps)
    train_df = pd.DataFrame(sin_seq, columns=['t', 'x', 'y'])
    pd_df_to_csv(train_df, 'sin5x-train-01.csv')
    #train_df.to_csv('sin5x-train-01.csv')

    test_seq = gen_sin_seq(total_steps)
    test_df = pd.DataFrame(sin_seq, columns=['t', 'x', 'y'])
    pd_df_to_csv(test_df, 'sin5x-test-01.csv')
    #test_df.to_csv('sin5x-test-01.csv')
