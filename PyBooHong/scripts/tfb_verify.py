import PyBooHong.esn.esn as esn
from PyBooHong.util.utils import *
import os.path
import pandas as pd
import torch
import matplotlib.pyplot as plt
from PyBooHong.scripts.md_config_0 import *

save_path = override_free_path(os.path.join(store_path, 'material/tfb_sample_00.csv'))

md_smpl = esn.SampleImitator(n_tch_in=n_tch_in,
                             n_out=n_out,
                             tau_out=tau_out)

state_env = torch.zeros(n_out)

history_smpl = []

for i in range(0, total_steps):
    tgt_env = tnsr_train_in_seq[i]
    tgt_env_value = tgt_env.numpy().item()
    state_env_value = state_env.numpy().item()
    err_value = state_env_value - tgt_env_value
    u_tch= gen_tch_in(state_env, tgt_env)
    u_tch_value = u_tch.numpy().item()
    history_smpl.append((i, i * dt, tgt_env_value, state_env_value, err_value, u_tch_value))
    state_env = md_smpl(dt, u_tch, state_env / c_out) * c_out
    state_env = state_env.clamp(min=-2.0, max=2.0).detach()


histroy_smpl_df = pd.DataFrame(history_smpl, columns = ['step','time', 'tgt_env', 'state_env', 'err', 'tch'])
histroy_smpl_df.to_csv(save_path)
#histroy_smpl_plt = histroy_smpl_df.plot(x='time', y=['tgt_env', 'state_env'])
