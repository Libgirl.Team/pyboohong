import PyBooHong.proc.pvltmn_ope as pvn_ope
import PyBooHong.esn.components as esn_cmpnts
import PyBooHong.esn.vltmn as vltmn

n_ii = 1
n_pr = 150
n_vr = 500
n_out = 1
genw_pr_pr = lambda tnsr: pvn_ope.genw_connection_forgetting(tnsr, 3.0e-1, n_pr)
genw_ti_pr = lambda tnsr: pvn_ope.genw_connection_forgetting(tnsr, 3.0e-1, 1)
genw_ei_pr = lambda tnsr: pvn_ope.genw_connection_forgetting(tnsr, 3.0e-1, 1)
gen_eqlbrm_pr = lambda tnsr_tau: pvn_ope.gen_equil_forgetting(tnsr_tau, -0.5, 0.3)
genw_pr_vr = lambda tnsr: pvn_ope.genw_connection_memorizing(tnsr, 3.0e-1, 0.8, n_pr + n_vr)
genw_vr_pr = lambda tnsr: pvn_ope.genw_connection_forgetting(tnsr, 3.0e-1, 1)
genw_pr_o = lambda tnsr: pvn_ope.genw_connection_forgetting(tnsr, 3.0e-1, 1)
genw_vr_vr = lambda tnsr: pvn_ope.genw_connection_memorizing(tnsr, 3.0e-1, 0.8, n_pr + n_vr)
genw_ti_vr = lambda tnsr: pvn_ope.genw_connection_memorizing(tnsr, 3.0e-1, 0.8, 1)
genw_ei_vr = lambda tnsr: pvn_ope.genw_connection_memorizing(tnsr, 3.0e-1, 0.8, 1)
genw_ii_vr = lambda tnsr: pvn_ope.genw_connection_memorizing(tnsr, 3.0e-1, 0.8, n_ii)
gen_eqlbrm_vr = lambda tnsr: pvn_ope.gen_equil_memorizing(tnsr, 0.05, 0., 1., 0.5)
tau_hebb = 4.0e+0
sgm_panic = 0.6
gen_tau_pr = lambda tnsr: esn_cmpnts.gen_tau_dyn(tnsr, [((3.,6.),0.75),((6.,15.),0.25)])
gen_tau_vr = lambda tnsr: esn_cmpnts.gen_tau_dyn(tnsr, [((3.,6.),0.70),((6.,15.),0.20),((15.,40.),0.05)])
fn_r = esn_cmpnts.steep_actv
state_low_bnd = -1.
state_up_bnd = 1.
w_low_bnd = -1.
w_up_bnd = 1.
hb_low_bnd = -1.
hb_up_bnd = 1.
rate_noise = 1000.

def md_v0():
    return vltmn.PVLTMN(n_ii = n_ii,
                        n_pr = n_pr,
                        n_vr = n_vr,
                        n_out = n_out,
                        genw_pr_pr = genw_pr_pr,
                        genw_ti_pr = genw_ti_pr,
                        genw_ei_pr = genw_ei_pr,
                        gen_eqlbrm_pr = gen_eqlbrm_pr,
                        genw_pr_vr = genw_pr_vr,
                        genw_vr_pr = genw_vr_pr,
                        genw_pr_o = genw_pr_o,
                        genw_vr_vr = genw_vr_vr,
                        genw_ti_vr = genw_ti_vr,
                        genw_ei_vr = genw_ei_vr,
                        genw_ii_vr = genw_ii_vr,
                        gen_eqlbrm_vr = gen_eqlbrm_vr,       
                        tau_hebb = tau_hebb,
                        sgm_panic = sgm_panic,
                        gen_tau_pr = gen_tau_pr,
                        gen_tau_vr = gen_tau_vr,
                        fn_r = fn_r,
                        state_low_bnd = state_low_bnd,
                        state_up_bnd = state_up_bnd,
                        w_low_bnd = w_low_bnd,
                        w_up_bnd = w_up_bnd,
                        hb_low_bnd = hb_low_bnd,
                        hb_up_bnd = hb_up_bnd,
                        rate_noise = rate_noise)
