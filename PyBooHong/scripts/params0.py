import os.path
import pbh_setting
import PyBooHong.y_seq.gen_y_segments as gy_seg
import logging
import pandas as pd
import torch

logger = logging.getLogger(__name__)

c_out = 2.0
dt = 0.1

train_y_seg_path = os.path.join(pbh_setting.store_path,'datasets/y_seq/y-segments-50x-train-00.csv')
train_y_seg_df = pd.read_csv(train_y_seg_path)
train_y_seq_df = gy_seg.gen_y_seq(dt, train_y_seg_df)

train_total_steps = train_y_seq_df['y'].size

logger.info('total number of training steps: {}'.format(train_total_steps))

tnsr_z_seq = (torch.tensor(train_y_seq_df['y'].map(lambda x: x / c_out))
              .reshape(train_total_steps, 1, -1).float())

logger.info('tnsr_z_seq.shape: {}'.format(tnsr_z_seq.shape))

