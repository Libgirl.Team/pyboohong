import logging
import pbh_setting
import PyBooHong.scripts.params0 as params0
import PyBooHong.scripts.md_params_0 as md_prms
import PyBooHong.proc.pvltmn_ope as pvn_ope
import torch
import torch.optim as optim

logger = logging.getLogger(__name__)

def exe():
    #logger.debug('try_exe successful! store_path: {}'.format(pbh_setting.store_path))
    md = md_prms.md_v0()
    pvn_ope.run_n_save(lambda md: pvn_ope.run_eval(md,
                                                   dt = params0.dt,
                                                   n_steps = params0.train_total_steps,
                                                   tnsr_z_seq_1out = params0.tnsr_z_seq,
                                                   is_train_tfb = True,
                                                   base_rate_panic = 2.0e-2,
                                                   rate_learn = 1.0e-3,
                                                   optm_fn = lambda params, rate: optim.SGD(params,
                                                                                            lr=rate,
                                                                                            momentum=0.9)),
                       md,
                       100,
                       pbh_setting.store_path,
                       'exp01_md',
                       'exp01_df')
