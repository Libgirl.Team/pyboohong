import PyBooHong
#import PyBooHong.starter

import logging
import importlib
import sys

logger = logging.getLogger(__name__)

logger.info('\n\n======================= Process start =======================\n')

try:
    for script in sys.argv[1:]:
        module_name = "PyBooHong.scripts.{}".format(script)
        script_module = importlib.import_module(module_name)
        script_module.exe()
except Exception as e:
    logger.exception(e)

logger.info('\n\n======================= Process end =======================\n')
